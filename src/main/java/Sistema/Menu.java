/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sistema;

import Animalitos.Ovejas;
import Back.AdminOvejas;
import java.util.ArrayList;
import java.util.Scanner;

public class Menu {

    static AdminOvejas adminOvejas = new AdminOvejas();

    public static void main(String[] args) {
        // declare e inicilice un scanner para capturar informacion mediante teclado
        // el nombre asignado es "teclado"
        Scanner teclado = new Scanner(System.in);

        ArrayList<Ovejas> listaOveja = new ArrayList<Ovejas>();

        int opcion;
        int respuesta;

        do {
            System.out.println("INGRESE LA OPCION ");
            System.out.println("");
            System.out.println("1_ Agregar Oveja");
            System.out.println("2_ Consultar informacion de la Oveja");
            System.out.println("3_ Eliminiar infomacion de la Oveja");
            System.out.println("");
            System.out.println("Ingrese su opcion aqui!");
            opcion = teclado.nextInt();
            System.out.println("");

            switch (opcion) {

                case 1:

                    do {

                        Ovejas oveja = new Ovejas();
                        System.out.println("agregando una Oveja");
                        System.out.println("");
                        System.out.println("ingrese el identificador de la Oveja");
                        int identificador = teclado.nextInt();
                        System.out.println("ingrese el nombre de la Oveja");
                        String nombre = teclado.next();
                        System.out.println("");
                        System.out.println("ingrese color de la Oveja");
                        String color = teclado.next();
                        System.out.println("");
                        System.out.println("ingrese genero de la Oveja");
                        String genero = teclado.next();
                        System.out.println("");

                        oveja.setIdentificador(identificador);
                        oveja.setNombre(nombre);
                        oveja.setColor(color);
                        oveja.setGenero(genero);

                        boolean resp = adminOvejas.agregarOveja(oveja);
                        if (resp == true) {
                            System.out.println("Informacion agregada corectamente");
                        } else {
                            System.out.println("Error al agragar los datos ");
                        }

                       
                        System.out.println("Información agregada correctamente ");
                        listaOveja.add(oveja);

                        System.out.println("¿Desea agregar mas Ovejas?? 1-si  2_no");
                        respuesta = teclado.nextInt();

                    } while (respuesta == 1);

                    break;

                case 2:

                    System.out.println("Ingrese el identificador de la Oveja");
                    int identificadorOvejas = teclado.nextInt();
                    boolean respConsultar = adminOvejas.consultarOvejas(identificadorOvejas);

                    if (respConsultar == false) {
                        System.out.println("Oveja no encontrada");
                    }

                    break;
                case 3:
                    do {

                        boolean respEliminacion = false;
                        System.out.println("Ingrese el identificador de la Oveja que quiere eliminar ");
                        int eliminarOvejas = teclado.nextInt();

                        System.out.println("");

                        boolean respEliminar = adminOvejas.consultarOvejas(eliminarOvejas);

                        if (respEliminar == false) {
                            System.out.println("Oveja no encontrada");
                        } else {
                            System.out.println("¿Seguro que quier eliminar los datos de la Ovejas?  1_si  2_no");
                            int confirmacion = teclado.nextInt();

                            if (confirmacion == 1) {
                                respEliminacion = adminOvejas.eliminarOvejas(eliminarOvejas);

                            }
                            if (respEliminacion == true) {
                                System.out.println("Oveja eliminada correctamente");
                            }
                        }

                        System.out.println("¿Desea eliminiar otra informacion de una Oveja?  1_si  2_no");
                        respuesta = teclado.nextInt();
                    } while (respuesta == 1);
                    break;

                default:
                    System.out.println("Opción inválida, gracias vuelva pronto");

            }
            System.out.println("");

            System.out.println("¿Desea volver al menú? 1_si  2_no");
            opcion = teclado.nextInt();

        } while (opcion == 1);

    }
}
