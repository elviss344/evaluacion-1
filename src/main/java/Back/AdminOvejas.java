/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Back;

import Animalitos.Ovejas;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AdminOvejas {

    ConexionBDD conexion = new ConexionBDD();
    Connection c;
    PreparedStatement ps;
    ResultSet rs;

    public boolean consultarOvejas(int identificador) {
        String sql = "SELECT * FROM animalitos WHERE identificador =" + "'" + identificador + "'";
        try {
            c = conexion.getConexion();
            ps = c.prepareStatement(sql);
            rs = ps.executeQuery();

            if (rs.next()) {
                System.out.println("El identificador de la Oveja es: " + rs.getInt("identificador"));
                System.out.println("el nombre de la Oveja es: " + rs.getNString("nombre"));
                System.out.println("el color de la Oveja es: " + rs.getNString("color"));
                System.out.println("el genero de la Oveja es: " + rs.getNString("genero"));
            } else {
                return false;
            }
        } catch (SQLException e) {
            System.out.println("Error al consultar la infomacion de la Oveja" + e.getMessage());
        }
        return true;
    }
    public boolean agregarOveja(Ovejas oveja){
        
        String sql = "INSERT INTO animalitos (identificador, nombre, color, genero) VALUES(?, ?, ?, ?)";
        try{
            c = conexion.getConexion();
            ps= c.prepareStatement(sql);
            ps.setInt(1, oveja.getIdentificador());
            ps.setString(2, oveja.getNombre());
            ps.setString(3, oveja.getColor());
            ps.setString(4, oveja.getGenero());
            
            ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
            System.out.println("Error al agregar la infomacion de la Oveja");
        
    }
     return true;  
    }
    
    public boolean eliminarOvejas(int identificador){
        
        String sql ="DELETE FROM animalitos WHERE identificador ="+"'"+identificador+"'";
        
        try{
            c = conexion.getConexion();
            ps= c.prepareStatement(sql);
           
            
            ps.executeUpdate();
            
          
        }catch (SQLException e){
            e.printStackTrace();
            System.out.println("Error al eliminar la infomacion de la vaca");
        
    }
        return true;
    }
}
