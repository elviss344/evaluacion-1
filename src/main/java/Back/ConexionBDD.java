/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Back;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexionBDD {
    private String BDD = "bddgranja";
    private String USER = "root";
    private String PASSWORD = "";
    private String URL = "jdbc:mysql://localhost:3306/"+BDD;
    private Connection connection = null;
    
    public Connection getConexion(){
        
        try{
            connection = (Connection)DriverManager.getConnection(this.URL,this.USER,this.PASSWORD);
            
        } catch(SQLException e) {
            System.out.println(e);
        }
        
        return connection;
    }
}
